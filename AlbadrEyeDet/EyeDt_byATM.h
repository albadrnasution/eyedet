
#ifndef __EYEDT_BYATM
#define __EYEDT_BYATM

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;



class EyeDt_byATM{

private:
	const int C_FOR_AdaptiveThreshold = 10;

public:
	Mat original;
	Point leftEyeLocation;
	Point rightEyeLocation;

	Mat imgContour, imgContourLeft, imgContourRight;
	Mat imgAfterThreshold;
	Mat imgEyewithLocation;
	Mat imgAllProcess;
	Mat statusLeft, statusRight;

	vector<Point> reContour, leContour;
	Point2f reMc, leMc;
	Moments reMu, leMu;

	bool leftEyeFound = false;
	bool rightEyeFound = false;


	Point findImageLoc_byMoment(Mat anEye, bool left){
		Mat drawing = Mat::zeros(anEye.size(), CV_8UC3);

		//Find countours 
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;
		findContours(anEye, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_L1, Point(0, 0));

		/// Get the moments
		vector<Moments> mu(contours.size());
		for (int i = 0; i < contours.size(); i++)
		{
			mu[i] = moments(contours[i], false);
		}

		///  Get the mass centers:
		vector<Point2f> mc(contours.size());
		for (int i = 0; i < contours.size(); i++)
		{
			mc[i] = Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
		}

		//count weighted value of eye x_center and y_center
		double sum_of_xwa = 0, sum_of_wa = 0, sum_of_ywa = 0;
		RNG rng(12345);
		for (int i = 0; i< contours.size(); i++)
		{
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			double cArea = contourArea(contours[i]);
			//process only contour blobs with big area
			if (cArea > 10){
				Rect br = boundingRect(contours[i]);
				//summation 
				sum_of_wa += br.width * cArea;
				sum_of_xwa += mc[i].x * br.width * cArea;
				sum_of_ywa += mc[i].y * br.width * cArea;
				//draw for show
				drawContours(drawing, contours, i, color, CV_FILLED, 8, hierarchy, 0, Point());
			}
		}

		//lastly, averaging from the sum
		int xc = (int)(sum_of_xwa / sum_of_wa);
		int yc = (int)(sum_of_ywa / sum_of_wa);
		Point2f determinedEyeCenter(xc, yc);

		//obtain the closest contour to the determinedEyeCenter
		double minDistance = INT_MAX;
		int closestCtr = -1;
		for (int i = 0; i < contours.size(); i++){
			double cArea = contourArea(contours[i]);
			if (cArea > 10){
				Point2f ey(xc, yc);
				Point diff = ey - mc[i];
				double distance = cv::sqrt(diff.x*diff.x + diff.y*diff.y);
				if (distance < minDistance){
					closestCtr = i;
					minDistance = distance;
				}
			}
		}

		//save to property
		RotatedRect rr;
		if (closestCtr != -1){
			rr = minAreaRect(contours[closestCtr]);
			Point2f rect_points[4]; rr.points(rect_points);
			for (int j = 0; j < 4; j++)
				line(drawing, rect_points[j], rect_points[(j + 1) % 4], Scalar(100, 100, 100), 1, 8);

			if (left){
				imgContourLeft = drawing.clone();
				leContour = contours[closestCtr];
				leMc = mc[closestCtr];
				leMu = mu[closestCtr];
				leftEyeFound = true;
			}
			else{//right
				imgContourRight = drawing.clone();
				reContour = contours[closestCtr];
				reMc = mc[closestCtr];
				reMu = mu[closestCtr];
				rightEyeFound = true;
			}
		}

		return determinedEyeCenter;
	}


	void combineAllProcessImage(){
		imgContour = Mat::zeros(original.size(), CV_8UC3);
		if (leftEyeFound)
			imgContourLeft.copyTo(imgContour(Rect(Point(0, 0), imgContourLeft.size())));

		if (rightEyeFound)
			imgContourRight.copyTo(imgContour(Rect(Point(imgContour.cols / 2 - 5, 0), imgContourRight.size())));

		//copy to all process
		imgAllProcess = Mat(original.size() * 2, CV_8UC3);

		original.copyTo(imgAllProcess(Rect(Point(0, 0), original.size())));

		Mat imgAfterThreshold_show = imgAfterThreshold.clone();
		cvtColor(imgAfterThreshold, imgAfterThreshold_show, CV_GRAY2RGB);

		imgAfterThreshold_show.copyTo(imgAllProcess(Rect(Point(original.cols, 0), original.size())));
		imgContour.copyTo(imgAllProcess(Rect(Point(0, original.rows), original.size())));
		imgEyewithLocation.copyTo(imgAllProcess(Rect(Point(original.cols, original.rows), original.size())));
	}

	void findEyes_fromImage(string openPath){
		resetProperty();
		string savePath = "procATM_" + openPath;
		//open image and put into property
		Mat imgEye = imread(openPath);
		original = imgEye.clone();

		//blur and convert to gray
		Mat imgEye_gray;
		blur(imgEye, imgEye, Size(3, 3));
		cvtColor(imgEye, imgEye_gray, CV_RGB2GRAY);

		//thresholding image
		Mat imgEye_at;
		adaptiveThreshold(imgEye_gray, imgEye_at, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 7, C_FOR_AdaptiveThreshold);
		imgAfterThreshold = imgEye_at.clone();

		//split image into left and right
		Mat iEyeLeft = imgEye_at(Rect(0, 0, imgEye_at.cols / 2, imgEye_at.rows));
		Mat iEyeRight = imgEye_at(Rect(imgEye_at.cols / 2, 0, imgEye_at.cols / 2, imgEye_at.rows));

		//find eye location
		leftEyeLocation = findImageLoc_byMoment(iEyeLeft, true);
		rightEyeLocation = findImageLoc_byMoment(iEyeRight, false);
		rightEyeLocation.x += imgEye_at.cols / 2;

		//draw location on original image;
		imgEyewithLocation = original.clone();
		if (leftEyeFound){
			line(imgEyewithLocation, Point(leftEyeLocation.x, leftEyeLocation.y - 5), Point(leftEyeLocation.x, leftEyeLocation.y + 5), Scalar(255, 255, 0), 1, 8);
			line(imgEyewithLocation, Point(leftEyeLocation.x - 5, leftEyeLocation.y), Point(leftEyeLocation.x + 5, leftEyeLocation.y), Scalar(255, 255, 0), 1, 8);
		}
		if (rightEyeFound){
			line(imgEyewithLocation, Point(rightEyeLocation.x, rightEyeLocation.y - 5), Point(rightEyeLocation.x, rightEyeLocation.y + 5), Scalar(255, 255, 0), 1, 8);
			line(imgEyewithLocation, Point(rightEyeLocation.x - 5, rightEyeLocation.y), Point(rightEyeLocation.x + 5, rightEyeLocation.y), Scalar(255, 255, 0), 1, 8);
		}

		//combine process
		combineAllProcessImage();
	}

	void resetProperty(){
		leftEyeFound = false;
		rightEyeFound = false;
		reContour.empty();
		leContour.empty();
	}


	void saveProcessImage(string savePath){
		imwrite(savePath, imgAllProcess);
	}

	string eyeOpenStatus(bool left){
		vector<Point> contour;
		Moments mu;
		if (left){
			contour = leContour;
			mu = leMu;
		}
		else{
			contour = reContour;
			mu = reMu;
		}

		RotatedRect rrle = minAreaRect(contour);
		double rrle_arcLength = arcLength(contour, true);
		double rrle_arcRatio = (rrle.size.width + rrle.size.height) / rrle_arcLength;
		double rrle_aspectRatio = (rrle.size.width > rrle.size.height) ? rrle.size.width / rrle.size.height : rrle.size.height / rrle.size.width;
		double rrle_areaRatio = mu.m00 / rrle.size.area();
		double totalWeight = 25 * rrle_arcRatio + 5 * rrle_aspectRatio + 25 * rrle_areaRatio;

		string status = "";
		if (totalWeight > 50) status = "closed";
		else if (totalWeight > 40 && totalWeight <= 50) status = "half";
		else if (totalWeight <= 40) status = "open";


		stringstream ss;
		ss << "Arc Ratio (Rect_Perimeter/Blob_Perimeter) =" << rrle_arcRatio << "\n";
		ss << "Aspect Ratio (W/H) = " << rrle_aspectRatio << "\n";
		ss << "Area Ratio (Blob/Rect) = " << rrle_areaRatio << "\n";
		ss << "TOTAL WEIGHT = " << totalWeight << "\n\n";
		ss << "STATUS: " << status;
		string allstat = ss.str();

		Mat stt(120, 250, CV_8UC3);

		ss.str(std::string()); ss << "Arc Ratio (Rect/Arc) =" << rrle_arcRatio;
		string arcRatio = ss.str();
		ss.str(std::string()); ss << "Aspect Ratio (W/H) = " << rrle_aspectRatio;
		string aspectRa = ss.str();
		ss.str(std::string()); ss << "Area Ratio (Blob/Rect) = " << rrle_areaRatio;
		string areaRatio = ss.str();
		ss.str(std::string()); ss << "TOTAL WEIGHT = " << totalWeight;
		string ttWeight = ss.str();
		ss.str(std::string()); ss << "STATUS: " << status;
		string strStatus = ss.str();

		putText(stt, arcRatio, cv::Point(10, 25), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
		putText(stt, aspectRa, cv::Point(10, 45), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
		putText(stt, areaRatio, cv::Point(10, 65), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
		putText(stt, ttWeight, cv::Point(10, 85), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
		putText(stt, strStatus, cv::Point(10, 105), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);

		if (left){
			statusLeft = stt;
		}
		else{
			statusRight = stt;
		}
		return ss.str();
	}

};


#endif 