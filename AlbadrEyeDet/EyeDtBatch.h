

#include "EyeDt_byATM.h";
#include "EyeDt_bycountour.h"


class EyeDtBatch{
public:
	void batchEyeDetectionExperiment(string folderPath){
		EyeDt_byATM edc;


		string openPath = folderPath;
		string savePath = "procATM_" + folderPath;
		_mkdir(savePath.c_str());

		DIR *dir;
		struct dirent *ent;
		if ((dir = opendir(folderPath.c_str())) != NULL) {
			/* print all the files and directories within directory */
			while ((ent = readdir(dir)) != NULL) {
				if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){}
				else{
					printf("Processing %s \n", ent->d_name);
					string imageName = string(ent->d_name);
					string imagePath = openPath + "\\" + imageName;
					string imageSavePath = savePath + "\\" + imageName;

					if (imageName.substr(imageName.find_last_of(".") + 1) == "bmp") {
						edc.findEyes_fromImage(imagePath);
						edc.saveProcessImage(imageSavePath);
					}
					else {
						std::cout << "> Not an image." << std::endl;
					}
				}
			}
			closedir(dir);
		}
		else {
			/* could not open directory */
			perror("");
		}
	}

};

