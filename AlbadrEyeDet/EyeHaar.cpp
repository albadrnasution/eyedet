#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;


int main(int argc, char *argv[]){

	Mat imgEye = imread(argv[1]);

	string fn_haar_eye = string("haarcascade_lefteye_2splits.xml");

	CascadeClassifier eye_cascade;
	eye_cascade.load(fn_haar_eye);


	// Clone the current frame:
	Mat original = imgEye.clone();
	// Convert the current frame to grayscale:
	Mat gray;
	cvtColor(original, gray, CV_BGR2GRAY);

	vector< Rect_<int> > rectEyes;
	eye_cascade.detectMultiScale(gray, rectEyes);

	for (Rect r : rectEyes){
		cout << "a";
		rectangle(gray, r, CV_RGB(0, 255, 0), 1);
	}

	imshow("Eye", gray);
	char key = (char)waitKey(0);
	// Exit this loop on escape:
	if (key == 27)
		return 0;
	return 0;
}
