#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>      // std::setw
#include "EyeDtBatch.h"
#include "EyeCloseOpen_basicThresholding.h"
using namespace cv;
using namespace std;


string eyeOpenStatus(vector<Point> contour, Moments mu){
	RotatedRect rrle = minAreaRect(contour);
	double rrle_arcLength = arcLength(contour, true);
	double rrle_arcRatio = (rrle.size.width + rrle.size.height) / rrle_arcLength;
	double rrle_aspectRatio = (rrle.size.width > rrle.size.height) ? rrle.size.width / rrle.size.height : rrle.size.height / rrle.size.width;
	double rrle_areaRatio = mu.m00 / rrle.size.area();
	double totalWeight = 25 * rrle_arcRatio + 5 * rrle_aspectRatio + 25 * rrle_areaRatio;

	string status = "";
	if (totalWeight > 50) status = "closed";
	else if (totalWeight > 40 && totalWeight <= 50) status = "half";
	else if (totalWeight <= 40) status = "open";

	return status;
}

void eyeAkeruTojiru(string folderPath){
	string openPath = folderPath;
	string savePath = "procCloseOpen_" + folderPath;
	_mkdir(savePath.c_str());
	EyeDt_byATM ecd;

	Mat veryBigMat(Size(1000, 700), CV_8UC3);

	cout << left << setw(8) << "Image" << "|" << setw(10) << "Arc/Rect" << "|" << setw(10) << "W/H=" << "|" << setw(10) << "B/R" << "|" << setw(12) << "WEIGHT" << endl;

	int i = 0;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(folderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){}
			else{
				//printf("Processing %s \n", ent->d_name);
				string imageName = string(ent->d_name);
				string imagePath = openPath + "\\" + imageName;

				if (imageName.substr(imageName.find_last_of(".") + 1) == "bmp") {
					ecd.findEyes_fromImage(imagePath);
					string imageName_noExt = imageName.substr(0, imageName.find_last_of("."));

					RotatedRect rrle = minAreaRect(ecd.leContour);
					double rrle_arcLength = arcLength(ecd.leContour, true);
					double rrle_arcRatio = (rrle.size.width + rrle.size.height) / rrle_arcLength;
					double rrle_aspectRatio = (rrle.size.width > rrle.size.height) ? rrle.size.width / rrle.size.height : rrle.size.height / rrle.size.width;
					double rrle_areaRatio = ecd.leMu.m00 / rrle.size.area();
					//double totalWeight = 6 * rrle_aspectRatio + 40 * rrle_areaRatio;
					double totalWeight = 25 * rrle_arcRatio + 5 * rrle_aspectRatio + 25 * rrle_areaRatio;
					

					cout << setw(8) << imageName_noExt << "|" << setw(10) << rrle_arcRatio << "|" << setw(10) << rrle_aspectRatio << "|" << setw(10) << rrle_areaRatio << "|" << setw(12) << totalWeight;

					//ecd.imgAllProcess.copyTo(veryBigMat(Rect(Point(0, 70 * i), ecd.imgAllProcess.size())));

					/*
					//cout << rrle.size.width << " / " << rrle.size.height << " = " << rrle_aspectRatio << endl;
					//cout << ecd.leMu.m00 << " / " << rrle.size.area() << " = " << rrle_areaRatio << endl;
					//cout << "======================================= Weight : " << totalWeight << endl;
					stringstream ss;
					ss << "L:" << ecd.leMu.m00 << " | " << ecd.leMu.m01 << " | " << ecd.leMu.m02;
					string text = ss.str();
					putText(veryBigMat, text, cv::Point(300, 70 * i + 20), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
					ss.str(std::string());
					ss << "R:" << ecd.reMu.m00 << " | " << ecd.reMu.m01 << " | " << ecd.reMu.m02;
					text = ss.str();
					putText(veryBigMat, text, cv::Point(300, 70 * i + 50), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
					*/

					//string status;
					//if (totalWeight > 50) status = "closed";
					//else if (totalWeight > 40 && totalWeight <= 50) status = "half";
					//else if (totalWeight <= 40) status = "open";


					string statusLeft = eyeOpenStatus(ecd.leContour, ecd.leMu);
					string statusRight = eyeOpenStatus(ecd.reContour, ecd.reMu);

					cout << setw(8) << statusLeft << " " << setw(8) << statusRight << endl;

					string imageSavePath = savePath + "\\" + imageName_noExt + " (" + statusLeft + "-" + statusRight + ").bmp";
					ecd.saveProcessImage(imageSavePath);
				}
				else {
					std::cout << "> Not an image." << std::endl;
				}

				i++;
				//if (i >= 10) break;
			}
		}
		closedir(dir);
		//imshow("Nur", veryBigMat);
	}
	else {
		/* could not open directory */
		perror("");
	}

}


int main(int argc, char *argv[]){

	///Is Eye Open Close
	//EyeCloseOpen_basicThresholding eco;
	//eco.processBatch(argv[1]);
	//eco.process();

	///Eye Position from one img
	//EyeDt_bycountour edc;
	//edc.findEyes_fromImage(string(argv[1]));
	//EyeDt_byATM edc;
	//edc.findEyes_fromImage(string(argv[1]));
	//imshow("Process", edc.imgAllProcess);
	
	///Eye Position batch
	//EyeDtBatch edb;
	//edb.batchEyeDetectionExperiment(argv[1]);

	eyeAkeruTojiru(argv[1]);


	// Exit this loop on escape:
	char key = (char)waitKey(0);
	if (key == 27)
		return 0;
	return 0;
}

