#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <direct.h>
#include "dirent.h"


using namespace cv;
using namespace std;

int ellipseWidth = 18;
int ellipseHeigt = 8;
vector<Mat> images;
vector<Point> leftEyeLoc;



/* 
 * 
 * 
 */
Point findDarkPlace_byBlockOverlappedHistogram(Mat maskedImage, int left = -1){
	int IMG_WIDTH_FULL = maskedImage.cols;
	int IMG_WIDTH_HALF = IMG_WIDTH_FULL / 2;
	int IMG_HEIGHT = maskedImage.rows;

	const int WINDOW_SIZE = 9;	  //boh window is 9x9
	const int WINDOW_STEP = 3;	  //window will slide with 3 step
	const int SMALLBOX_SIZE = 3;  //box within the boh window is 3x3
	const int NW_COLS = (IMG_WIDTH_HALF - WINDOW_SIZE) / WINDOW_STEP;		//total number of boh window horizontally
	const int NW_ROWS = (IMG_HEIGHT - WINDOW_SIZE) / WINDOW_STEP;			//total number of boh window vertically

	double min_boh_value = INT_MAX;
	Point  min_boh_loctn(0, 0);

	//find the darkest box of 9x9 window size with 3 pixels step
	for (int i = NW_COLS - 1; i >= 0; i--){
		for (int j = 0; j < NW_ROWS; j++){
			// location of inspected boh window
			int xstart = i * WINDOW_STEP;
			int ystart = j * WINDOW_STEP;
			// if we want to find the right side of image, shift location to half right
			if (left > 0) xstart += IMG_WIDTH_HALF;

			// trim the window
			Mat window = maskedImage(Rect(xstart, ystart, WINDOW_SIZE, WINDOW_SIZE));
			// compute summary of pixels within window
			Scalar boh_sum = sum(window);
			// compare to currently saved maximum
			if (boh_sum.val[0] < min_boh_value){
				min_boh_value = boh_sum.val[0];
				min_boh_loctn = Point(i, j);
			}
		}
	}

	//using the darkest boh, find the darkest point within
	int xpos_darkestWindow = 3 * min_boh_loctn.x;
	int ypos_darkestWindow = 3 * min_boh_loctn.y;

	// if we want to find in the right side of image, shift location to half right
	if (left > 0) xpos_darkestWindow += IMG_WIDTH_HALF;

	//the darkest boh window
	Mat darkest_boh_window = maskedImage(Rect(xpos_darkestWindow, ypos_darkestWindow, WINDOW_SIZE, WINDOW_SIZE));

	double darkest_value = INT_MAX;
	Point  darkest_point(-1, -1);

	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			//trim the block
			Mat block = darkest_boh_window(Rect(i * SMALLBOX_SIZE, j * SMALLBOX_SIZE, SMALLBOX_SIZE, SMALLBOX_SIZE));
			Scalar bsum = sum(block);

			//because dark (black) is zero, minimum is darker
			if (bsum.val[0] < darkest_value){
				darkest_value = bsum.val[0];
				int darkx = xpos_darkestWindow + i * SMALLBOX_SIZE + 1;
				int darky = ypos_darkestWindow + j * SMALLBOX_SIZE + 1;
				darkest_point = Point(darkx, darky);
			}
		}
	}

	return darkest_point;
}


/*
* Find coloumn with the darkest sum of pixels value and
* also row with the darkest sum of pixels value
*
*/
Point findDarkPlace_byHVHistogramProjection(Mat maskedImage){
	//// find the vertical projection
	vector<double> ver_proj; // holds the column sum values    
	double min_ver_proj_val = INT_MAX; // holds the minimum value    
	int    min_ver_proj_loc = 0; // holds the location of minimum value    
	for (int i = 0; i < maskedImage.cols; ++i)
	{
		Mat col = maskedImage.col(i);			// get individual columns    
		Scalar col_sum = sum(col);				// find the sum of ith column
		ver_proj.push_back(col_sum.val[0]);		// push back to vector
		// in the case of black=0, darkest means minimum
		if (col_sum.val[0] < min_ver_proj_val){
			min_ver_proj_val = col_sum.val[0];
			min_ver_proj_loc = i;
		}
	}
	//// find the horizontal projection
	vector<double> hor_proj; // holds the column sum values    
	double min_hor_proj_val = INT_MAX; // holds the minimum value    
	int    min_hor_proj_loc = 0; // holds the location of minimum value    
	for (int i = 0; i < maskedImage.rows; ++i)
	{
		Mat row = maskedImage.row(i);			// get individual columns    
		Scalar row_sum = sum(row);				// find the sum of ith column
		hor_proj.push_back(row_sum.val[0]);		// push back to vector
		// in the case of black=0, darkest means minimum
		if (row_sum.val[0] < min_hor_proj_val){
			min_hor_proj_val = row_sum.val[0];
			min_hor_proj_loc = i;
		}
	}
	return Point(min_ver_proj_loc, min_hor_proj_loc);
}

Point findDarkPlace(Mat maskedImage, int left = -1){
	return findDarkPlace_byBlockOverlappedHistogram(maskedImage, left);
	//return findDarkPlace_byHVHistogramProjection(maskedImage);
}

Mat trimImage_withEllipse(Mat inputImage, vector<RotatedRect> ellipses){
	Mat imgEyeMask_left = inputImage.clone();
	Mat zero = Mat::zeros(inputImage.size(), CV_8UC3);

	zero.setTo(Scalar(255, 255, 255));
	//combine all eclipse on left eye
	for (int i = 0; i < ellipses.size(); ++i){
		ellipse(zero, ellipses[i], Scalar(0, 0, 0), CV_FILLED, 8);
	}
	bitwise_or(inputImage, zero, imgEyeMask_left);

	return imgEyeMask_left;
}

void processOneImage(String openPath){

	String savePath = "processed_" + openPath;
	Mat imgEye = imread(openPath);
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	// Clone the current frame:
	Mat imgEyeClone = imgEye.clone();
	// Convert the current frame to grayscale:
	Mat src_gray;
	cvtColor(imgEyeClone, src_gray, CV_BGR2GRAY);
	blur(src_gray, src_gray, Size(3, 3));

	Mat canny_output;
	/// Detect edges using canny
	Canny(src_gray, canny_output, 100, 100 * 2, 3);

	//Find countours 
	findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_L1, Point(0, 0));

	Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
	Mat allEllipse = imgEye.clone();
	Mat blank = Mat::zeros(imgEye.size(), CV_8UC3);

	vector<RotatedRect> minEllipse(contours.size());
	vector<RotatedRect> eyeLikeEllipse_left;
	vector<RotatedRect> eyeLikeEllipse_right;

	//cout << "Ellipse`s Angle:" << endl;

	RNG rng(12345);
	for (int i = 0; i< contours.size(); i++)
	{
		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		drawContours(drawing, contours, i, color, 1, 8, hierarchy, 0, Point());

		//contour that can be fitted to ellipse
		if (contours[i].size() > 5){
			minEllipse[i] = fitEllipse(contours[i]);
			//draw ellipse on all ellipse canvas
			ellipse(allEllipse, minEllipse[i], color, 1, 8);

			//just putting angle of ellipse on the image
			char output[50];
			sprintf(output, "%3.0f", minEllipse[i].angle);
			String angle = String(output);
			//cout << "n_points: " << contours[i].size() << " - angle: " << angle << endl;
			putText(blank, angle, minEllipse[i].center + Point2f(-10, 5), FONT_HERSHEY_SIMPLEX, 0.4, color, 1, 8);

			//draw only if angle of ellipse between 70-110
			if (minEllipse[i].angle > 60 && minEllipse[i].angle < 120){
				// draw ellipse on original image
				ellipse(imgEyeClone, minEllipse[i], color, 1, 8);

				//put the ellipse to right or left based on its center point x axis
				if (minEllipse[i].center.x < imgEye.cols / 2)
					eyeLikeEllipse_left.push_back(minEllipse[i]);
				else
					eyeLikeEllipse_right.push_back(minEllipse[i]);
			}
		}
	}


	//// ============================================================== HANDLE THE LEFT EYE
	//trim image with the ellipse found before
	Mat imgEyeMask_left = trimImage_withEllipse(imgEye, eyeLikeEllipse_left);

	//Finding the iris, the most dark location
	Point dark_lloc = findDarkPlace(imgEyeMask_left, -1);
	//cout << "Eye point on the left :�@" << dark_lloc << endl;


	//// ============================================================== HANDLE THE RIGHT EYE
	//trim image with the ellipse found before
	Mat imgEyeMask_right = trimImage_withEllipse(imgEye, eyeLikeEllipse_right);

	//Finding the iris, the most dark location
	Point dark_rloc = findDarkPlace(imgEyeMask_right, +1);
	//cout << "Eye point on the right :�@" << dark_rloc << endl;


	//// ============================================================== DRAWING
	Scalar leftColor = Scalar(255, 0, 0);
	Scalar rightColor = Scalar(255, 0, 0);


	if (eyeLikeEllipse_left.size() != 0 && eyeLikeEllipse_right.size()!=0){
		//when ellipses are found on both eyes
	}
	else if (eyeLikeEllipse_left.size() != 0 && eyeLikeEllipse_right.size() == 0){
		//when ellipses are found only on left side of the image
		dark_rloc = Point(imgEye.cols - dark_lloc.x, dark_lloc.y);
		rightColor = Scalar(0, 0, 255);
	}
	else if (eyeLikeEllipse_left.size() == 0 && eyeLikeEllipse_right.size() != 0){
		//when ellipses are found only on right side of the image
		dark_lloc = Point(imgEye.cols - dark_rloc.x, dark_rloc.y);
		leftColor = Scalar(0, 0, 255);
	}

	//draw line with each eyemask
	line(imgEyeMask_left, Point(dark_lloc.x, 0), Point(dark_lloc.x, imgEyeMask_left.rows - 1), leftColor, 1, 8);
	line(imgEyeMask_left, Point(0, dark_lloc.y), Point(imgEyeMask_left.cols - 1, dark_lloc.y), leftColor, 1, 8);
	line(imgEyeMask_right, Point(dark_rloc.x, 0), Point(dark_rloc.x, imgEyeMask_right.rows - 1), rightColor, 1, 8);
	line(imgEyeMask_right, Point(0, dark_rloc.y), Point(imgEyeMask_right.cols - 1, dark_rloc.y), rightColor, 1, 8);

	//Draw + + on original image of eye
	Mat imgFinal = imgEye.clone();
	line(imgFinal, Point(dark_lloc.x, dark_lloc.y - 5), Point(dark_lloc.x, dark_lloc.y + 5), leftColor, 1, 8);
	line(imgFinal, Point(dark_lloc.x - 5, dark_lloc.y), Point(dark_lloc.x + 5, dark_lloc.y), leftColor, 1, 8);
	line(imgFinal, Point(dark_rloc.x, dark_rloc.y - 5), Point(dark_rloc.x, dark_rloc.y + 5), rightColor, 1, 8);
	line(imgFinal, Point(dark_rloc.x - 5, dark_rloc.y), Point(dark_rloc.x + 5, dark_rloc.y), rightColor, 1, 8);



	//// ============================================================== COMBINE AND SAVING IMAGE 
	Mat saveImage = Mat::zeros(imgEye.size() * 3, CV_8UC3);
	Mat src_gray_d, edge_d;
	cvtColor(src_gray, src_gray_d, CV_GRAY2RGB);
	cvtColor(canny_output, edge_d, CV_GRAY2RGB);

	src_gray_d.copyTo(saveImage(Rect(cv::Point(0, 0), imgEye.size())));
	edge_d.copyTo(saveImage(Rect(cv::Point(imgEye.cols, 0), imgEye.size())));
	drawing.copyTo(saveImage(Rect(cv::Point(imgEye.cols * 2, 0), imgEye.size())));
	allEllipse.copyTo(saveImage(Rect(cv::Point(0, imgEye.rows), imgEye.size())));
	blank.copyTo(saveImage(Rect(cv::Point(imgEye.cols, imgEye.rows), imgEye.size())));
	imgEyeClone.copyTo(saveImage(Rect(cv::Point(imgEye.cols * 2, imgEye.rows), imgEye.size())));
	imgEyeMask_left.copyTo(saveImage(Rect(cv::Point(0, imgEye.rows * 2), imgEye.size())));
	imgFinal.copyTo(saveImage(Rect(cv::Point(imgEye.cols, imgEye.rows * 2), imgEye.size())));
	imgEyeMask_right.copyTo(saveImage(Rect(cv::Point(imgEye.cols * 2, imgEye.rows * 2), imgEye.size())));
	imwrite(savePath, saveImage);


	ellipse(imgFinal, dark_lloc, Size(ellipseWidth, ellipseHeigt), 0, 0, 360, Scalar(255, 0, 0), 1, 8);



	//// ============================================================== Show in a window
	namedWindow("Contours", CV_WINDOW_AUTOSIZE);
	imshow("Blur", src_gray);
	imshow("Canny", canny_output);
	imshow("Contours", drawing);
	imshow("All Ellips", allEllipse);
	imshow("Ellips", imgEyeClone);
	imshow("Angles", blank);
	imshow("Left Experiment", imgEyeMask_left);
	imshow("Right Experiment", imgEyeMask_right);
	imshow("Final", imgFinal);
	imshow("Saving", saveImage);
}

void processBatch(string folderPath){
	string openPath = folderPath;
	string savePath = "processed_" + folderPath;
	_mkdir(savePath.c_str());

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(folderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			if (strcmp(ent->d_name, ".")==0 || strcmp(ent->d_name, "..")==0){}
			else{
				printf("Processing %s \n", ent->d_name);
				string imageName = string(ent->d_name);
				string imagePath = openPath + "\\" + imageName;

				if (imageName.substr(imageName.find_last_of(".") + 1) == "bmp") {
					processOneImage(imagePath);
				}
				else {
					std::cout << "> Not an image." << std::endl;
				}
				/*std::string::size_type pos = imageName.find('.bmp');
				if (pos != std::string::npos){
					cout << "" << pos;
					processOneImage(imagePath);
				}*/
			}
		}
		closedir(dir);
	}
	else {
		/* could not open directory */
		perror("");
	}
}

int main(int argc, char *argv[]){


	//processOneImage(string(argv[1]));
	//processBatch(string(argv[1]));
	

	waitKey(0);
}

