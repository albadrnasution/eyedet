#pragma once
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "EyeDt_byATM.h"
#include "EyeDt_byCountour.h"
#include <msclr\marshal_cppstd.h>
#include <iostream>
#include <fstream>
#include <sstream>

namespace AlbadrEyeDet {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace msclr::interop;

	/// <summary>
	/// MainForm の概要
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;

	private: System::Windows::Forms::PictureBox^  pbx;
	private: System::Windows::Forms::ToolStripMenuItem^  functionToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  trimToolStripMenuItem;
	private: System::Windows::Forms::PictureBox^  pbcr;
	private: System::Windows::Forms::ToolStripMenuItem^  processToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  processEllipseToolStripMenuItem;


	protected:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->functionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->trimToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->processToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->processEllipseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->pbx = (gcnew System::Windows::Forms::PictureBox());
			this->pbcr = (gcnew System::Windows::Forms::PictureBox());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbcr))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->functionToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(320, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->openToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(36, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(102, 22);
			this->openToolStripMenuItem->Text = L"Open...";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::openToolStripMenuItem_Click);
			// 
			// functionToolStripMenuItem
			// 
			this->functionToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->trimToolStripMenuItem,
					this->processToolStripMenuItem, this->processEllipseToolStripMenuItem
			});
			this->functionToolStripMenuItem->Name = L"functionToolStripMenuItem";
			this->functionToolStripMenuItem->Size = System::Drawing::Size(61, 20);
			this->functionToolStripMenuItem->Text = L"Function";
			// 
			// trimToolStripMenuItem
			// 
			this->trimToolStripMenuItem->Name = L"trimToolStripMenuItem";
			this->trimToolStripMenuItem->Size = System::Drawing::Size(149, 22);
			this->trimToolStripMenuItem->Text = L"Trim";
			this->trimToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::trimToolStripMenuItem_Click);
			// 
			// processToolStripMenuItem
			// 
			this->processToolStripMenuItem->Name = L"processToolStripMenuItem";
			this->processToolStripMenuItem->Size = System::Drawing::Size(149, 22);
			this->processToolStripMenuItem->Text = L"Process Blob";
			this->processToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::processToolStripMenuItem_Click);
			// 
			// processEllipseToolStripMenuItem
			// 
			this->processEllipseToolStripMenuItem->Name = L"processEllipseToolStripMenuItem";
			this->processEllipseToolStripMenuItem->Size = System::Drawing::Size(149, 22);
			this->processEllipseToolStripMenuItem->Text = L"Process Ellipse";
			this->processEllipseToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainForm::processEllipseToolStripMenuItem_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// pbx
			// 
			this->pbx->Cursor = System::Windows::Forms::Cursors::Cross;
			this->pbx->Location = System::Drawing::Point(0, 27);
			this->pbx->Name = L"pbx";
			this->pbx->Size = System::Drawing::Size(320, 240);
			this->pbx->TabIndex = 2;
			this->pbx->TabStop = false;
			this->pbx->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MainForm::pbx_Paint);
			this->pbx->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::pbx_MouseDown);
			this->pbx->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::pbx_MouseMove);
			// 
			// pbcr
			// 
			this->pbcr->Location = System::Drawing::Point(0, 285);
			this->pbcr->Name = L"pbcr";
			this->pbcr->Size = System::Drawing::Size(320, 71);
			this->pbcr->TabIndex = 3;
			this->pbcr->TabStop = false;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(320, 360);
			this->Controls->Add(this->pbcr);
			this->Controls->Add(this->pbx);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbx))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pbcr))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		System::Drawing::Rectangle rect;
		public: Bitmap^ image1;

	private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
				 {
					 System::String^ filename = openFileDialog1->FileName;
					 this->Text = openFileDialog1->SafeFileName;
					 					 
					 // Retrieve the image.
					 image1 = gcnew Bitmap(filename, true);
					 // Set the PictureBox to display the image.
					 pbx->Image = image1;
				 }
	}

	private: System::Void pbx_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 rect = System::Drawing::Rectangle(e->X, e->Y, 0, 0);
				 pbx->Invalidate();
	}

	private: System::Void pbx_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

				 if (e->Button == System::Windows::Forms::MouseButtons::Left)
				 {
					 rect = System::Drawing::Rectangle(rect.Left, rect.Top, e->X - rect.Left, e->Y - rect.Top);

					 if (rect.Height > 30){
						 //e->Y = rect.Top + 30;
						 rect.Height = 30;
					 }
				 }
				 pbx->Invalidate();
	}

	private: System::Void pbx_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
				 // Create a new pen.
				 Pen^ skyBluePen = gcnew Pen(Brushes::Red);

				 // Set the pen's width.
				 skyBluePen->Width = 1.0F;

				 // Set the LineJoin property.
				 skyBluePen->LineJoin = System::Drawing::Drawing2D::LineJoin::Bevel;

				 // Draw a rectangle.
				 e->Graphics->DrawRectangle(skyBluePen, rect);

				 //Dispose of the pen. 
				 delete skyBluePen;

	}

	private: System::Void trimToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 try
				 {
					 Bitmap^ cropped = image1->Clone(rect, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
					 pbcr->Image = cropped;
					 cropped->Save("temp.bmp");
				 }
				 catch (System::Exception^ ex){
					 MessageBox::Show("No image or no rectangle provided.");
				 }
	}
	private: System::Void processToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 EyeDt_byATM edt;

				 edt.findEyes_fromImage("temp.bmp");
				 if (edt.rightEyeFound || edt.leftEyeFound){
				 }
				 else{
					 Mat noEye(edt.imgAllProcess.size(), CV_8UC3);
					 putText(edt.imgAllProcess, "NO EYES", cv::Point(10, 40), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 0, 255), 2, 8);
				 }
				 imshow("Processing Flow (Blob)", edt.imgAllProcess);
				 moveWindow("Processing Flow (Blob)", 500, 200);


				 destroyWindow("Blob - Left Eye");
				 destroyWindow("Blob - Right Eye");

				 if (edt.leftEyeLocation.x >= 0 && edt.leftEyeLocation.y >= 0){
					 string statusLeft = edt.eyeOpenStatus(true);
					 imshow("Blob - Left Eye", edt.statusLeft);
					 moveWindow("Blob - Left Eye", 500, 300);
				 }
				 if (edt.rightEyeLocation.x >= 0 && edt.rightEyeLocation.y >= 0){
					 string statusRight = edt.eyeOpenStatus(false);
					 imshow("Blob - Right Eye", edt.statusRight);
					 moveWindow("Blob - Right Eye", 780, 300);
				 }

				 //marshal_context^ context = gcnew marshal_context();
				 //System::String^ resultLeft = context->marshal_as<System::String^>(statusLeft);
				 //MessageBox::Show(resultLeft);

	}
	private: System::Void processEllipseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 EyeDt_bycountour edt;

				 edt.findEyes_fromImage("temp.bmp");
				 imshow("Processing Flow (Ellipse)", edt.processImage);
				 moveWindow("Processing Flow (Ellipse)", 500, 500);

				 edt.eyeOpenStatus();
	}
};
}
