#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include "dirent.h"

using namespace cv;
using namespace std;

/// Global variables

int threshold_value = 0;
int threshold_type = 3;;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;

Mat src, src_gray, dst;
char* window_name = "Threshold Demo";

char* trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
char* trackbar_value = "Value";

/// Function headers
void Threshold_Demo(int, void*);


Mat processBatch(string folderPath){
	string openPath = folderPath;
	Mat veryBigMat(Size(520, 240), CV_8UC3);

	int i = 0;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(folderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){}
			else{
				printf("Processing %s \n", ent->d_name);
				string imageName = string(ent->d_name);
				string imagePath = openPath + "\\" + imageName;

				if (imageName.substr(imageName.find_last_of(".") + 1) == "bmp") {

					Mat pic = imread(imagePath);
					if (pic.cols>130)
						pic = pic(Rect(0, 0, 130, 30));

					pic.copyTo(veryBigMat(Rect(cv::Point(130 * (i % 4), 30 * (i / 4)), pic.size())));
					cout << cv::Point(120 * (i % 4), 30 * (i / 4)) << endl;
				}
				else {
					std::cout << "> Not an image." << std::endl;
				}

				std::cout << i;
				i++;
				if (i > 16) break;
			}
		}
		closedir(dir);
	}
	else {
		/* could not open directory */
		perror("");
	}
	return veryBigMat;
}

/**
* @function main
*/
int main(int argc, char** argv)
{
	/// Load an image
	//src = imread(argv[1], 1);

	src = processBatch(argv[1]);

	/// Convert the image to Gray
	cvtColor(src, src_gray, CV_RGB2GRAY);

	/// Create a window to display results
	namedWindow(window_name, CV_WINDOW_AUTOSIZE);

	/// Create Trackbar to choose type of Threshold
	createTrackbar(trackbar_type,
		window_name, &threshold_type,
		max_type, Threshold_Demo);

	createTrackbar(trackbar_value,
		window_name, &threshold_value,
		max_value, Threshold_Demo);

	/// Call the function to initialize
	Threshold_Demo(0, 0);

	/// Wait until user finishes program
	while (true)
	{
		int c;
		c = waitKey(20);
		if ((char)c == 27)
		{
			break;
		}
	}

}


/**
* @function Threshold_Demo
*/
void Threshold_Demo(int, void*)
{
	/* 0: Binary
	1: Binary Inverted
	2: Threshold Truncated
	3: Threshold to Zero
	4: Threshold to Zero Inverted
	*/

	threshold(src_gray, dst, threshold_value, max_BINARY_value, threshold_type);

	imshow(window_name, dst);
}