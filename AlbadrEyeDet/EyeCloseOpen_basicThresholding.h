#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include "EyeDt_bycountour.h"
using namespace cv;
using namespace std;


class EyeCloseOpen_basicThresholding{

	int ellipseWidth = 16;
	int ellipseHeigt = 7;
	vector<Mat> images;
	vector<Point> leftEyeLoc;
	vector<Point> rightEyeLoc;
public:

	Mat trimImage_withEllipse(Mat inputImage, RotatedRect ellipses){
		Mat imgEyeMask_left = inputImage.clone();
		Mat zero = Mat::zeros(inputImage.size(), CV_8UC3);

		zero.setTo(Scalar(255, 255, 255));
		//combine all eclipse on left eye
		ellipse(zero, ellipses, Scalar(0, 0, 0), CV_FILLED, 8);

		Mat dst_show;
		cvtColor(inputImage, dst_show, CV_GRAY2RGB);
		bitwise_or(dst_show, zero, imgEyeMask_left);

		return imgEyeMask_left;
	}

	void process(){
		Mat veryBigMat(Size(1000, 800), CV_8UC3);

		for (int i = 0; i < images.size(); i++){
			Mat src = images[i];
			Mat src_gray, dst;
			Point eyeL = leftEyeLoc[i];
			Point eyeR = rightEyeLoc[i];
			/// Convert the image to Gray
			cvtColor(src, src_gray, CV_RGB2GRAY);

			threshold(src_gray, dst, 180, 255, 0);

			src.copyTo(veryBigMat(Rect(cv::Point(0, 40 * i), src.size())));

			Mat dst_show;
			cvtColor(dst, dst_show, CV_GRAY2RGB);
			ellipse(dst_show, eyeL, Size(ellipseWidth, ellipseHeigt), 0, 0, 360, Scalar(255, 0, 0), 1, 8);
			ellipse(dst_show, eyeR, Size(ellipseWidth, ellipseHeigt), 0, 0, 360, Scalar(255, 0, 0), 1, 8);

			dst_show.copyTo(veryBigMat(Rect(cv::Point(150, 40 * i), dst.size())));

			RotatedRect elipsL(eyeL, Size(2 * ellipseWidth, 2 * ellipseHeigt), 0);
			Mat elipsOnlyLeft = trimImage_withEllipse(dst, elipsL);
			elipsOnlyLeft = elipsOnlyLeft(Rect(0, 0, elipsOnlyLeft.cols / 2, elipsOnlyLeft.rows));
			elipsOnlyLeft.copyTo(veryBigMat(Rect(cv::Point(300, 40 * i), elipsOnlyLeft.size())));

			RotatedRect elipsR(eyeR, Size(2 * ellipseWidth, 2 * ellipseHeigt), 0);
			Mat elipsOnlyRight = trimImage_withEllipse(dst, elipsR);
			elipsOnlyRight = elipsOnlyRight(Rect(elipsOnlyRight.cols / 2, 0, elipsOnlyRight.cols / 2, elipsOnlyRight.rows));
			elipsOnlyRight.copyTo(veryBigMat(Rect(cv::Point(300 + elipsOnlyLeft.cols, 40 * i), elipsOnlyRight.size())));

			cvtColor(elipsOnlyLeft, elipsOnlyLeft, CV_RGB2GRAY);
			cvtColor(elipsOnlyRight, elipsOnlyRight, CV_RGB2GRAY);

			int elipsArea = (int)(3.1415 * ellipseHeigt * ellipseWidth);
			int blackLeft = elipsOnlyLeft.cols * elipsOnlyLeft.rows - countNonZero(elipsOnlyLeft);
			int blackRight = elipsOnlyRight.cols * elipsOnlyRight.rows - countNonZero(elipsOnlyRight);
			double ratioLeft = (double)blackLeft / elipsArea;
			double ratioRight = (double)blackRight / elipsArea;

			string statusL = "opened";
			if (ratioLeft <= 0.4) statusL = "closed";
			else if (ratioLeft > 0.4 && ratioLeft <= 0.5) statusL = "maybe closed";
			string statusR = "opened";
			if (ratioRight <= 0.4) statusR = "closed";
			else if (ratioRight > 0.4 && ratioRight <= 0.5) statusR = "maybe closed";


			stringstream ss;
			ss << blackLeft << " (" << ratioLeft << ") " << statusL << " | " << blackRight << " (" << ratioRight << ") " << statusR;
			string text = ss.str();

			putText(veryBigMat, text, cv::Point(450, 40 * i + 12), FONT_HERSHEY_SIMPLEX, 0.4, Scalar(0, 0, 255), 1, 8);
		}

		imshow("Thres test", veryBigMat);
	}

	void processBatch(string folderPath){
		string openPath = folderPath;
		EyeDt_bycountour ecd;

		int i = 0;
		DIR *dir;
		struct dirent *ent;
		if ((dir = opendir(folderPath.c_str())) != NULL) {
			/* print all the files and directories within directory */
			while ((ent = readdir(dir)) != NULL) {
				if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){}
				else{
					printf("Processing %s \n", ent->d_name);
					string imageName = string(ent->d_name);
					string imagePath = openPath + "\\" + imageName;

					if (imageName.substr(imageName.find_last_of(".") + 1) == "bmp") {
						ecd.findEyes_fromImage(imagePath);
						Point l = ecd.leftEyeLocation;
						Point r = ecd.rightEyeLocation;
						Mat m = ecd.imgFinal;
						images.push_back(m);
						leftEyeLoc.push_back(l);
						rightEyeLoc.push_back(r);
					}
					else {
						std::cout << "> Not an image." << std::endl;
					}

					i++;
					if (i >= 20) break;
				}
			}
			closedir(dir);
		}
		else {
			/* could not open directory */
			perror("");
		}

	}

};